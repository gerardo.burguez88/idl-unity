﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class CharacterController : MonoBehaviour{
    public Font fontDamange;

    protected Rigidbody2D       rb2d;
    protected Animator          animator;
    protected Collider2D[]      contactsPoint;
    protected SpellController   spell;

    public string[]     animationAttack;
    public string       animationIdle;
    public string       animationWalk;
    public string       animationDead;

    public float        attrVelocity        = 0.8f;
    public float        attrMaxLife         = 15f;
    public float        attrLife            = 15f;
    public float        attrDamage          = 5f;
    public float        attrSpellDamage     = 5f;
    public float        attrCriticalChance  = 5f;
    public float        attrDefend          = 0f;
    public float        attrSpellDefend     = 0f;
    public float        attrAgility         = 1.0f;
    public float        attrAttackRate      = 2.0f;
    public float        attrForce           = 1f;

    public float        attrLevel           = 1f;
    public float        attrGold            = 0f;
    public float        attrExp             = 0f;
    
    protected float     attackFlag          = 0f;
    protected float     paralyzedFlag       = 0f;
    protected float     paralyzedCc         = 5f;
    
    private float       forceTime           = 1f;
    private float       forceChangeValue    = 0.2f;

    public enum CharacterState{Move,MoveStop,Fight,Dead,WaitForDestroy,ProtagonistIsDead,Paralyzed};
    public CharacterState state = CharacterState.Move;

    void Awake(){
        rb2d     = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spell    = GameObject.Find("GeneratorSpell").GetComponent<SpellController>();
    }

    protected virtual void FixedUpdate(){
        ForceDecrease();
    }

    protected void ForceDecrease(){
        if (attrForce > 1f && Time.time > forceTime){
            attrForce = attrForce - forceChangeValue;
            forceTime = Time.time + 1f;
            Debug.Log(attrForce);
        }
    }

    public void ForceIncrease(){
        if (attrForce < 2f){
            attrForce = attrForce + forceChangeValue;
            Debug.Log(attrForce);
        }
    }

    protected void WaitToChangeAnimation(string play = ""){
        if (play != null){
            bool isOk = true;
            for (int index = 0; index < animationAttack.Length; index++){
                if (animator.GetCurrentAnimatorStateInfo(0).IsName(animationAttack[index])){
                    isOk = false;
                }
            }
            if(isOk == true){
                animator.PlayInFixedTime(play);
            }            
        }
    }

    public void EffectParalyzed(float second){
        if (paralyzedFlag == 0 || paralyzedFlag + paralyzedCc < Time.time){
            paralyzedFlag = Time.time + second;
        }
    }
    
    public void PhysicalDamage(GameObject source){
        if (state != CharacterState.Dead && state != CharacterState.WaitForDestroy && state != CharacterState.ProtagonistIsDead){
            CharacterController ccSource    = source.GetComponent<CharacterController>();
            float xMaxDamage                = ccSource.attrDamage * ccSource.attrForce;
            float xCriticalChance           = ccSource.attrCriticalChance;
            float xMinDamage                = Mathf.Round(xMaxDamage * 0.7f);
            float xDamage                   = Mathf.Round(Random.Range(xMinDamage,xMaxDamage));
            bool isCrit                     = false;

            if (Random.Range(1,100) <= (int)xCriticalChance){
                isCrit  = true;
                xDamage = xDamage * 2;
            }

            if (attrLife - xDamage <= 0){
                attrLife = 0f;
                state = CharacterState.Dead;
            }else{
                attrLife = attrLife - xDamage;
            }

            StartCoroutine(TextLife(xDamage,isCrit,0.3f));
        }            
    }

    public void SpellDmage(GameObject source, float spellModifier){
        if (state != CharacterState.Dead && state != CharacterState.WaitForDestroy && state != CharacterState.ProtagonistIsDead){
            CharacterController ccSource    = source.GetComponent<CharacterController>();
            float xMaxDamage                = ccSource.attrSpellDamage + spellModifier;
            float xCriticalChance           = ccSource.attrCriticalChance;
            float xMinDamage                = Mathf.Round(xMaxDamage * 0.7f);
            float xDamage                   = Mathf.Round(Random.Range(xMinDamage,xMaxDamage));
            bool isCrit                     = false;

            if (Random.Range(1,100) <= (int)xCriticalChance){
                isCrit  = true;
                xDamage = xDamage * 2;
            }
            
            if (attrLife - xDamage <= 0){
                attrLife = 0f;
                state = CharacterState.Dead;
            }else{
                attrLife = attrLife - xDamage;
            }

            StartCoroutine(TextLife(xDamage,isCrit,0.3f));
        }
    }

    public void PhysicalHealing(GameObject source){
        if (state != CharacterState.Dead && state != CharacterState.WaitForDestroy && state != CharacterState.ProtagonistIsDead){
            if (attrMaxLife > attrLife){
                float xHealing = 1f;
                attrLife = attrLife + xHealing;
                StartCoroutine(TextLife(xHealing,false,0.3f,false));
            }
        }            
    }

    public void SpellHealing(GameObject source, float spellModifier){
        if (state != CharacterState.Dead && state != CharacterState.WaitForDestroy && state != CharacterState.ProtagonistIsDead){
            CharacterController ccSource    = source.GetComponent<CharacterController>();
            float xMaxHealing               = ccSource.attrSpellDamage + spellModifier;
            float xCriticalChance           = ccSource.attrCriticalChance;
            float xMinHealing               = Mathf.Round(xMaxHealing * 0.7f);
            float xHealing                  = Mathf.Round(Random.Range(xMinHealing,xMaxHealing));
            bool isCrit                     = false;

            if (Random.Range(1,100) <= (int)xCriticalChance){
                isCrit  = true;
                xHealing = xHealing * 2;
            }
            
            if (attrLife + xHealing > attrMaxLife){
                attrLife = attrMaxLife;
            }else{
                attrLife = attrLife + xHealing;
            }

            StartCoroutine(TextLife(xHealing,isCrit,0.3f,false));
        }
    }

    IEnumerator TextLife(float xPoint, bool isCrit, float delay = 0,bool xIsDamage = true){
        if (delay != 0)
           yield return new WaitForSeconds(delay);

        GameObject newGo = new GameObject("CanvasInfo");
        
        Canvas canvas = newGo.AddComponent<Canvas>();
        canvas.sortingLayerName ="Character";

        CanvasScaler canvasScale = newGo.AddComponent<CanvasScaler>();
        canvasScale.dynamicPixelsPerUnit = 4;

        newGo.AddComponent<GraphicRaycaster>();
        newGo.transform.SetParent(gameObject.transform);

        RectTransform rectTransform = newGo.GetComponent<RectTransform>();
        rectTransform.localPosition  = new Vector3(0f,0f,1f);
        rectTransform.localScale     = new Vector3(1f,1f,1f);
        rectTransform.sizeDelta = new Vector2(2,2);

        GameObject newGo2 = new GameObject("Damage");
        Text text = newGo2.AddComponent<Text>();
        newGo2.transform.SetParent(newGo.transform);
        text.fontStyle = FontStyle.Bold;
        text.fontSize = 10;
        text.alignment = TextAnchor.MiddleCenter;
        text.font = fontDamange;
        if (xIsDamage){
            text.color = (isCrit ? Color.yellow : Color.white);    
        }else{
            text.color = (isCrit ? Color.blue : Color.green);
        }
        text.text = (xIsDamage ? "- " : "+ ") + xPoint.ToString() + (isCrit ? " Crit" : "");
        
        float xDirection = (gameObject.tag == "Enemy" ? 1 : -1);

        rectTransform = newGo2.GetComponent<RectTransform>();
        rectTransform.localPosition  = new Vector3((xDirection * 0.4f),0.4f,0f);
        rectTransform.localScale     = new Vector3(0.02f,0.02f,1f);
        rectTransform.sizeDelta      = new Vector2(100,20);

        float effectDuration    = 1f;
        float effectEnd         = Time.time + effectDuration;
        float effectCicle       = effectDuration / 100f;
        float decreseAlpha      = text.color.a / 100f;
        float moveText          = 0.1f / 100f;

        while(effectEnd > Time.time && text.color.a > 0){
            rectTransform.localPosition = new Vector3(rectTransform.localPosition.x + (xDirection * moveText),rectTransform.localPosition.y + moveText,0f);
            text.color = new Color(text.color.r,text.color.g,text.color.b,text.color.a - decreseAlpha);
            yield return new WaitForSeconds(effectCicle);
        }

        Destroy(newGo);
    }    
}