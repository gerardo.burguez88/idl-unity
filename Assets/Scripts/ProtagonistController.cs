﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProtagonistController : CharacterController{

    private BackgroundController backgroundUi;

    void Start(){
        backgroundUi = GameObject.Find("BackgroundGame").GetComponent<BackgroundController>();
    }

    protected override void FixedUpdate(){
        base.FixedUpdate();
        UpdateUi();
        CheckState();
        if (state == CharacterState.Move){
            Move();    
        }else if(state == CharacterState.MoveStop){
            MoveStop();
        }else if(state == CharacterState.Fight){
            Fight();
        }else if(state == CharacterState.Dead){
            Dead();
        }else if(state == CharacterState.Paralyzed){
            Paralyzed();
        }
    }

    void UpdateUi(){
        backgroundUi.ChangeText("life",attrMaxLife,attrLife);
        backgroundUi.ChangeText("attrDamage",attrDamage);
        backgroundUi.ChangeText("attrSpellDamage",attrSpellDamage);
        backgroundUi.ChangeText("attrCriticalChance",attrCriticalChance);
        backgroundUi.ChangeText("attrDefend",attrDefend);
        backgroundUi.ChangeText("attrSpellDefend",attrSpellDefend);
        backgroundUi.ChangeText("attrAgility",attrAgility);
    }

    void CheckState(){
        animator.speed = 1f;
        if (state != CharacterState.Dead && state != CharacterState.WaitForDestroy){
            if (paralyzedFlag > Time.time){
                state = CharacterState.Paralyzed;
            }else{
                state = CharacterState.Move;
                contactsPoint = new Collider2D[20];
                rb2d.GetContacts(contactsPoint);
                for (int index = 0; index < contactsPoint.Length; index++){
                    if (contactsPoint[index] != null){
                        if (state != CharacterState.Fight && contactsPoint[index].gameObject.tag == "LimitMiddle"){
                            state = CharacterState.MoveStop;
                        }
                        if (contactsPoint[index].gameObject.tag == "Enemy"){
                            state = CharacterState.Fight;
                        }
                    }
                }
            }
        }            
    }

    void Fight(){
        GameManager.instance.parallaxActive = false;

        bool isOk = true;
        if (attackFlag == 0f || Time.time > attackFlag){
            contactsPoint = new Collider2D[20];
            rb2d.GetContacts(contactsPoint);
            for (int index = 0; index < contactsPoint.Length; index++){
                if (contactsPoint[index] != null && contactsPoint[index].gameObject.tag == "Enemy"){
                    EnemyController enemy = contactsPoint[index].gameObject.GetComponent<EnemyController>();
                    animator.PlayInFixedTime(animationAttack[Random.Range(0,animationAttack.Length)]);
                    spell.Cast("Healing",gameObject);
                    spell.Cast("Fire",gameObject);
                    enemy.PhysicalDamage(gameObject);
                    isOk = false;
                }
            }
            attackFlag = Time.time + attrAttackRate;
        }
        if (isOk){
            WaitToChangeAnimation(animationIdle);
        }
    }

    void Move(){
        GameManager.instance.parallaxActive = false;
        WaitToChangeAnimation(animationWalk);
        rb2d.velocity = new Vector2(1 * attrVelocity,0f);
        attackFlag = 0f;
    }    

    void MoveStop(){
        GameManager.instance.parallaxActive = true;
        WaitToChangeAnimation(animationWalk);
        attackFlag = 0f;
    }

    void Paralyzed(){
        GameManager.instance.parallaxActive = false;
        animator.speed = 0f;
        attackFlag = Time.time + attrAttackRate;
    }

    void Dead(){
        GameManager.instance.parallaxActive = false;
        animator.PlayInFixedTime(animationDead);

        GameObject[] enemyGoList = GameObject.FindGameObjectsWithTag("Enemy");
        EnemyController enemy;
        foreach (GameObject enemyGo in enemyGoList){
            enemy = enemyGo.GetComponent<EnemyController>();
            if (enemy.state != EnemyController.CharacterState.WaitForDestroy){
                enemy.state = EnemyController.CharacterState.ProtagonistIsDead;    
            }
        }

        state = CharacterState.WaitForDestroy;
    }
}
