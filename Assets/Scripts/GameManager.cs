﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour{
    public float parallaxSpeed = 0.02f;
    public RawImage forest4;
    public RawImage forest3;
    public RawImage forest2;
    public RawImage forest1;
    public RawImage forest0;
    public RawImage light1;
    public RawImage light2;
    public RawImage grass1;
    public RawImage grass2;
    
    private GeneratorEnemyController enemyGenerator;
    private ProtagonistController protagonistController;

    public static GameManager instance = null;
    public bool parallaxActive = false;

    void Start(){
    }

    void Awake(){
    	if (instance == null)
			instance = this;
		else if (instance != null)
			Destroy (gameObject);

        enemyGenerator          = GameObject.Find("GeneratorEnemy").GetComponent<GeneratorEnemyController>();
        protagonistController   = GameObject.FindGameObjectsWithTag("Protagonist")[0].GetComponent<ProtagonistController>();

        enemyGenerator.StartGenerator();
    }

    void FixedUpdate(){
        if (parallaxActive){
            Parallax();    
        }
        
        if (protagonistController.state == CharacterController.CharacterState.WaitForDestroy){
            enemyGenerator.CancelGenerator();
        }
    }

    void Parallax(){
        float finalSpeed    = parallaxSpeed * Time.deltaTime;
        forest4.uvRect      = new Rect(forest4.uvRect.x + finalSpeed, 0f, 1f, 1f);
        forest3.uvRect      = new Rect(forest3.uvRect.x + finalSpeed * 0.5f, 0f, 1f, 1f);
        forest2.uvRect      = new Rect(forest2.uvRect.x + finalSpeed * 1f, 0f, 1f, 1f);
        forest1.uvRect      = new Rect(forest1.uvRect.x + finalSpeed * 1.5f, 0f, 1f, 1f);
        forest0.uvRect      = new Rect(forest0.uvRect.x + finalSpeed * 2f, 0f, 1f, 1f);
        light1.uvRect       = new Rect(light1.uvRect.x + finalSpeed, 0f, 1f, 1f);
        light2.uvRect       = new Rect(light2.uvRect.x + finalSpeed, 0f, 1f, 1f);
        grass1.uvRect       = new Rect(grass1.uvRect.x + finalSpeed, 0f, 1f, 1f);
        grass2.uvRect       = new Rect(grass2.uvRect.x + finalSpeed * 0.5f, 0f, 1f, 1f);        
    }       
}
