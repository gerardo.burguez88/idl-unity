﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneratorEnemyController : MonoBehaviour{

    public GameObject[] enemyPrefab;
    private float generatorTimer = 8f;    

    public void StartGenerator(){
        InvokeRepeating("CreateEnemy",0f,generatorTimer);
    }

    public void CancelGenerator(bool clean = false){
        CancelInvoke("CreateEnemy");
/*         if (clean){
            Object[] allEnemies = GameObject.FindGameObjectsWithTag("Enemy");
            foreach (GameObject enemy in allEnemies){
                Destroy(enemy);
            }
        } */
    }    
    
    void CreateEnemy(){
        GameObject toInstantiate = enemyPrefab[Random.Range (0,enemyPrefab.Length)];
        Instantiate(toInstantiate,transform.position,Quaternion.identity);
    }
}
