﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BackgroundController : MonoBehaviour{
    public GameObject   life;
    public GameObject   attrDamage;
    public GameObject   attrSpellDamage;
    public GameObject   attrCriticalChance;
    public GameObject   attrDefend;
    public GameObject   attrSpellDefend;
    public GameObject   attrAgility;

    public GameObject   spellHealing;
    public GameObject   spellFire;
    public GameObject   spellFrost;
    public GameObject   spellThunder;

    public GameObject   potion;
    public GameObject   force;

    private ProtagonistController protagonist;

    void Awake(){
        potion.GetComponent<Button>().onClick.AddListener(PotionClick);
        force.GetComponent<Button>().onClick.AddListener(ForceClick);

//        spellHealing.GetComponent<Button>().onClick.AddListener(PotionClick);
  //      spellFire.GetComponent<Button>().onClick.AddListener(PotionClick);
    //    spellFrost.GetComponent<Button>().onClick.AddListener(PotionClick);
       // spellThunder.GetComponent<Button>().onClick.AddListener(PotionClick);
    }

    void Start(){
        protagonist = GameObject.FindGameObjectWithTag("Protagonist").GetComponent<ProtagonistController>();
    }

    public void ChangeText(string attrText,float attrValue){
        string stringAttrValue = attrValue.ToString();

        switch (attrText){
            case "attrDamage":
                attrDamage.GetComponentInChildren<Text>().text = stringAttrValue;
                break;
            case "attrSpellDamage":
                attrSpellDamage.GetComponentInChildren<Text>().text = stringAttrValue;
                break;
            case "attrCriticalChance":
                attrCriticalChance.GetComponentInChildren<Text>().text = stringAttrValue;
                break;
            case "attrDefend":
                attrDefend.GetComponentInChildren<Text>().text = stringAttrValue;
                break;
            case "attrSpellDefend":
                attrSpellDefend.GetComponentInChildren<Text>().text = stringAttrValue;
                break;
            case "attrAgility":
                attrAgility .GetComponentInChildren<Text>().text = stringAttrValue;
                break;                                                                                
            default:
                break;
        }
    }

    public void ChangeText(string attrText,float attrValue,float attrValue2){
        string stringAttrValue = attrValue.ToString();
        string stringAttrValue2= attrValue2.ToString();

        switch (attrText){
            case "life":
                life.GetComponentInChildren<Text>().text = stringAttrValue+"HP/"+stringAttrValue2+"HP";
                break;
            default:
                break;
        }
    }

    void PotionClick(){
        protagonist.PhysicalHealing(protagonist.gameObject);
    }

    void ForceClick(){
        protagonist.ForceIncrease();
    }

    void SpellClick(string spell){

    }
}
