﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellController : MonoBehaviour{
    public GameObject   spellPrefab;
    public float        spellDamageHealing  = 0f;
    public float        spellDamageFire     = 0f;

    public void Cast(string spell, GameObject source){
        if (spell == "Healing"){
            StartCoroutine(Healing(source));
        }

        if (spell == "Fire"){
            string tag = (source.tag == "Protagonist" ? "Enemy" : "Protagonist");
            GameObject[] enemyGoList = GameObject.FindGameObjectsWithTag(tag);
            foreach (GameObject enemyGo in enemyGoList){
                StartCoroutine(Fire(source,enemyGo));
            }      
        }

        if (spell == "Frost"){
            string tag = (source.tag == "Protagonist" ? "Enemy" : "Protagonist");

            Collider2D[]  contactsPoint = new Collider2D[20];
            source.GetComponent<Rigidbody2D>().GetContacts(contactsPoint);

            foreach (Collider2D collider in contactsPoint){
                if (collider != null && collider.gameObject.tag == tag){
                    StartCoroutine(Frost(source,collider.gameObject));
                }
            }
        }        
    }

    Transform Clone(GameObject target, string spell, Vector3 scale){
        GameObject newGo = Instantiate(spellPrefab,target.transform.position,Quaternion.identity);
        newGo.transform.SetParent(target.transform);
        newGo.GetComponent<Animator>().Play(spell);

        Transform newGoTransform  = newGo.GetComponent<Transform>();
        newGoTransform.localScale = scale;

        return newGoTransform;
    }

    IEnumerator Healing(GameObject source){
        Transform newGoTransform = Clone(source,"SpellHealing",new Vector3(0.4f,0.4f,1f));

        CharacterController target = source.GetComponent<CharacterController>();
        target.SpellHealing(source,spellDamageHealing);

        float effectDuration    = 0.5f;
        float effectEnd         = Time.time + effectDuration;
        float effectCicle       = effectDuration / 100f;
        float decreaseAxi       = newGoTransform.localScale.x / 100f ;
        while(effectEnd > Time.time && newGoTransform.localScale.x > 0){
            newGoTransform.localScale = new Vector3(newGoTransform.localScale.x - decreaseAxi,newGoTransform.localScale.y - decreaseAxi,1f);
            yield return new WaitForSeconds(effectCicle);
        }

        Destroy(newGoTransform.gameObject);
    }

    IEnumerator Fire(GameObject source, GameObject enemyGo){
        Transform newGoTransform = Clone(enemyGo,"SpellFire",new Vector3(1.5f,1.5f,1f));

        CharacterController target = enemyGo.GetComponent<CharacterController>();
        target.SpellDmage(source,spellDamageFire);

        float effectDuration    = 0.5f;
        float effectEnd         = Time.time + effectDuration;
        float effectCicle       = effectDuration / 100f;
        float decreaseAxi       = newGoTransform.localScale.x / 100f ;
        while(effectEnd > Time.time && newGoTransform.localScale.x > 0){
            //newGoTransform.localScale = new Vector3(newGoTransform.localScale.x - decreaseAxi,newGoTransform.localScale.y - decreaseAxi,1f);
            yield return new WaitForSeconds(effectCicle);
        }

        Destroy(newGoTransform.gameObject);
    }   

    IEnumerator Frost(GameObject source, GameObject enemyGo){
        Transform newGoTransform = Clone(enemyGo,"SpellFrost",new Vector3(1f,1f,1f));

        CharacterController target = enemyGo.GetComponent<CharacterController>();
        target.SpellDmage(source,spellDamageFire);
        target.EffectParalyzed(3f);

        float effectDuration    = 0.5f;
        float effectEnd         = Time.time + effectDuration;
        float effectCicle       = effectDuration / 100f;
        float decreaseAxi       = newGoTransform.localScale.x / 100f ;
        while(effectEnd > Time.time && newGoTransform.localScale.x > 0){
            //newGoTransform.localScale = new Vector3(newGoTransform.localScale.x - decreaseAxi,newGoTransform.localScale.y - decreaseAxi,1f);
            yield return new WaitForSeconds(effectCicle);
        }

        Destroy(newGoTransform.gameObject);
    }      
}
