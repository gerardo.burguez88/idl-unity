﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : CharacterController{
    private Collider2D collider2d;

    void Start(){
        collider2d  = GetComponent<BoxCollider2D>();

        string[] tagsToDisable = {"Enemy","EnemyDead"};
        foreach (string tag in tagsToDisable){
            GameObject[] enemyGoList = GameObject.FindGameObjectsWithTag(tag);
            foreach (GameObject enemyGo in enemyGoList){
                Physics2D.IgnoreCollision(enemyGo.GetComponent<Collider2D>(),collider2d);
            }    
        }
    }

    protected override void FixedUpdate(){
        CheckState();
        if (state == CharacterState.Move){
            Move();    
        }else if(state == CharacterState.Fight){
            Fight();
        }else if(state == CharacterState.Dead){
            Dead();
        }else if(state == CharacterState.WaitForDestroy){
            WaitForDestroy();
        }else if(state == CharacterState.ProtagonistIsDead){
            ProtagonistIsDead();
        }else if(state == CharacterState.Paralyzed){
            Paralyzed();
        }
    }

    void CheckState(){
        animator.speed = 1f;
        if (state !=CharacterState.Dead && state !=CharacterState.WaitForDestroy && state != CharacterState.ProtagonistIsDead){
            if (paralyzedFlag > Time.time){
                state = CharacterState.Paralyzed;
            }else{
                state =CharacterState.Move;
                contactsPoint = new Collider2D[20];
                rb2d.GetContacts(contactsPoint);
                for (int index = 0; index < contactsPoint.Length; index++){
                    if (contactsPoint[index] != null){
                        if (contactsPoint[index].gameObject.tag == "Protagonist"){
                            state = CharacterState.Fight;
                        }
                    }
                }
            }
        }
    }

    void Fight(){
        bool isOk = true;
        if (attackFlag == 0f || Time.time > attackFlag){
            contactsPoint = new Collider2D[20];
            rb2d.GetContacts(contactsPoint);
            for (int index = 0; index < contactsPoint.Length; index++){
                if (contactsPoint[index] != null && contactsPoint[index].gameObject.tag == "Protagonist"){
                    ProtagonistController protagonist = contactsPoint[index].gameObject.GetComponent<ProtagonistController>();
                    animator.PlayInFixedTime(animationAttack[Random.Range(0,animationAttack.Length)]);
                    protagonist.PhysicalDamage(gameObject);
                    isOk = false;
                }
            }            
            attackFlag = Time.time + attrAttackRate;    
        }
        if (isOk){
            WaitToChangeAnimation(animationIdle);
        }
    }

    void Move(){
        WaitToChangeAnimation(animationWalk);
        if (GameManager.instance.parallaxActive){
            ProtagonistController protagonist = GameObject.FindGameObjectsWithTag("Protagonist")[0].GetComponent<ProtagonistController>();
            rb2d.velocity = new Vector2(-1 * (attrVelocity + protagonist.attrVelocity),0f);    
        }else{
            rb2d.velocity = new Vector2(-1 * (attrVelocity),0f);    
        }
    }

    void Paralyzed(){
        animator.speed = 0f;
        attackFlag = Time.time + attrAttackRate;
    }

    void Dead(){
        gameObject.tag = "EnemyDead";
        animator.PlayInFixedTime(animationDead);

        Collider2D protagonist = GameObject.FindGameObjectsWithTag("Protagonist")[0].GetComponent<BoxCollider2D>();
        Physics2D.IgnoreCollision(protagonist,collider2d);
        
        state =CharacterState.WaitForDestroy;
    }

    void WaitForDestroy(){
        ProtagonistController protagonist = GameObject.FindGameObjectsWithTag("Protagonist")[0].GetComponent<ProtagonistController>();
        if (protagonist.state == ProtagonistController.CharacterState.Move || protagonist.state == ProtagonistController.CharacterState.MoveStop){
            rb2d.velocity = new Vector2(-1 * protagonist.attrVelocity,0f);    
        }
    }

    void ProtagonistIsDead(){
        WaitToChangeAnimation(animationIdle);
    }

    void OnCollisionEnter2D(Collision2D Collision){
        if (Collision.gameObject.tag == "Destroyer"){
            Destroy(gameObject);
        }
    }    
}
